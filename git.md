# An introduction to git

As programmers, we deal with projects containing hundreds of files, and we modify those files hundreds of times; many times, several programmers are working on the same files; to keep track of all this, we use [Version Control](https://en.wikipedia.org/wiki/Version_control).

One of the most popular version control systems is [Git](https://en.wikipedia.org/wiki/Git). The Git specification defines how to store all the history, and the `git` command is used to modify this history (there are many other commands and graphical tools to view and modify git repositories).

Git stores files, along with the history of all the changes in a *repository*. git repositories can exist in different computers, and can be synchronized with each other.

Although git is designed as a distributed version control system, with no repository being the primary or master, it is many times convenient for us to designate one repository in one machine as the master, and then have every team member synchronize with it. Several companies offer free hosting of git repositories, along wiht web access to the files, and the ability to do code reviews; the most popular is probably [github](https://github.com), but I prefer to use [GitLab](https://gitlab.com), since it provides us with unlimited private repositories.

## Git concepts

### repositories

The set of files managed by git is called a git **repository**. A repository consists of :
- a folder on your computer (and all its files and subfolders)
- configuration files (.gitignore and .git/config )
- a hidden folder containing all the different versions of your files.

Usually you have your own repository on your local computer (and may have different copies on different computers), and you synchronize to  a *remote* repository in an agreed upon central server (like gitlab or github, for example). The default name given to the remote repository is *origin* (the name is just to identify it within commands, and has nothing to do with the server). You could have more than one remote in a repo.

### commits

A **commit** is a set of changes to files; git commits have:
* a unique identifier (hash)
* a comment
* a set of files, with changes

git keeps track of the state of files by applying chains of commits.

### branches

A git branch is a named set of commits. We grow branches by adding commits to it (`git commit`); We create a branch from the current state of another branch (`git checkout -b ...`); as different branches get different commits, they allow us to keep different versions of a repo.

Keep in mind that branches with the same name in different repos can be out of sync (or have completely different info, which we strongly advise against :)

There is a default branch on each repo, usually called *master*.

### merging

We keep branches in sync with *merge* operations, which apply all commits from one branch to another. You can usually *push* your changes to a remote branch, as long as nobody else has added commits to it.

For the master branch, we normally merge in the remote repo (i.e. gitlab), by sending a *merge request* or *pull request*, and merging through the web UI.

## File states and operations
There are 3 states a file can be in git:
- **committed** -  Git has the latest version of the file
- **modified** - you've modified it in your folder, but git doesn't 'know' about the changes
- **staged** - You've modified the file and told git about it, but haven't committed it yet.


![file states in git](gitStates.png)

### git subcommands
* git branch
* git add
* git checkout
* git pull
* git merge
* git push

## Resources
* [Git main site](https://git-scm.com/)
* [Pro git](https://git-scm.com/book/en/v2)
