# Git demo

* Both Dr Rich and I have downloaded copies of the repo, and our remote/origin points to the same repo on gitlab (`git clone `)
* Look at the file numbers.txt
    * notice we're missing line for 3
* Dr Rich will send a PR for adding 3
    * change to right folder, GitDemo
    * create branch line3   `git checkout -b line3`
    * modify file (and check status `git status`)
    * stage modified file (`git add numbers.txt`)
    * commit (`git commit -m "line 3"`)
    * push to gitlab (`git push origin line3`)
    * create PR
* I can look at PR, merge
* We both pull to our repo, go back to master
    * `git checkout master`
    * `git pull`
* now let's do a merge conflict
* Dr Rich will create a branch, called line6, and add line 6
    * create branch line6   `git checkout -b line6`
    * modify file (and check status `git status`)
    * stage modified file (`git add numbers.txt`)
    * push to gitlab (`git push origin line6`)
    * create PR
* I will create a branch called line7
    * create branch line6   `git checkout -b line7`
    * modify file (and check status `git status`)
    * stage modified file (`git add numbers.txt`)
    * push to gitlab (`git push origin line7`)
    * create PR
* These two PRs are in conflict (modify the same file, in ways that conflict)
    * if I merge one, the other one can't be merged
    * demo it

